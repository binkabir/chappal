Chappal is a simple network application for monitoring uptime of servers.

Usage: 
1. Download the application and use ```sbt``` to compile it. 
2. Use the `POST` endpoint http://localhost:8080/site with a json structure 
 ```{ "url": "https://powershop.ng", "name": "powershop" , "mobileNumber": "08068869000"}```
 to add website for monitoring. 
 3. Use the get endpoint ```http://localhost:8080/site/<name>``` to view the http log response
   of the server been monitored. 
   
 
