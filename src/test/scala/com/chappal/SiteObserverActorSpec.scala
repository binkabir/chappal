package com.chappal

import akka.actor.{ActorSystem, Cancellable, Props }
import akka.testkit.{  TestKit, TestProbe }

import org.scalatest.{ BeforeAndAfterAll, WordSpec }
import com.chappal.actors.SiteObserverActor
import com.chappal.actors.SiteObserverActor._

import scala.collection.mutable.ListMap


final class SiteObserverActorSpec extends WordSpec with BeforeAndAfterAll {

  private implicit val system = ActorSystem("HttpMessengerActorSpec")

  val sender = TestProbe()
  implicit val ec = system.dispatcher
  implicit val senderRef = sender.ref

  val siteObserverActor = system.actorOf(Props(new SiteObserverActor("https://powershop.ng", "powershop")), "powershop")

  "Site Observer" must {
    """ 1. add a http Messenger actor ref to its monitor list when it receives an Observer object
        2. Pause a Site observer monitoring
        3. return the site name been monitored.
      """ in {

      siteObserverActor ! Observe
      siteObserverActor ! Pause("powershop")

      siteObserverActor ! ServerList

      assert(sender.receiveN(1).size == 1 && sender.expectMsgType[ListMap[String, Cancellable]].exists(_._1 == "powershop"))

    }

    "should return the List of accumulated logs in a type Future[Seq[PingResponse]]]" in {

      siteObserverActor ! GetStatus("powershop")

      sender.receiveN(1)
    }
  }

  override protected def afterAll() = {

    TestKit.shutdownActorSystem(system)
  }

}
