package com.chappal

import com.chappal.actors.HeartBeatReceptionistActor.ActionPerformed
import com.chappal.actors.HttpMessengerActor.PingResponse
import com.chappal.actors.Website

//#json-support
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.DefaultJsonProtocol

trait JsonSupport extends SprayJsonSupport {
  // import the default encoders for primitive types (Int, String, Lists etc)
  import DefaultJsonProtocol._

  implicit val websiteJsonFormat = jsonFormat3(Website)
  implicit val pingResponseJsonFormat = jsonFormat2(PingResponse)
  implicit val actionPerformedJsonFormat = jsonFormat1(ActionPerformed)
}
//#json-support
