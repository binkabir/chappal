package com.chappal.actors

import akka.actor.{ Actor, ActorLogging, ActorRef, Props }
import akka.util.Timeout
import com.chappal.actors.HttpMessengerActor.PingResponse
import com.chappal.actors.SiteObserverActor._

import scala.concurrent.Future

object HeartBeatReceptionistActor {
  final case class CreateObserver(website: Website)
  final case class ActionPerformed(description: String)
  final case class HeartBeat(name: String)

  def props: Props = Props[HeartBeatReceptionistActor]

}

class HeartBeatReceptionistActor extends Actor with ActorLogging {
  import HeartBeatReceptionistActor._

  var websites = Set.empty[Website]
  import akka.pattern.ask
  import scala.concurrent.duration._
  implicit val ec = context.system.dispatcher
  implicit val timeout = Timeout(3 seconds)

  override def receive: Receive = {

    case CreateObserver(website) =>
      websites.find(_.url == website.url) match {
        case Some(w) => ActionPerformed("website already exists")
        case None =>
          websites += website
          val siteObserverActor = context.actorOf(Props(new SiteObserverActor(website.url, website.name)), website.name)

          siteObserverActor ! Observe
          sender() ! ActionPerformed(s"successfully added ${website.name} for monitoring ")
        //   }

      }

    case HeartBeat(name) =>

      val m = websites.find(_.name == name).flatMap(w => context.child(w.name).map(act => getWebsiteStatus(act, name)))

      sender() ! m.map(_.flatten)

  }

  private def getWebsiteStatus(actor: ActorRef, name: String): Future[Future[Seq[PingResponse]]] = {
    actor.ask(GetStatus(name)).mapTo[Future[Seq[PingResponse]]]
  }

}
