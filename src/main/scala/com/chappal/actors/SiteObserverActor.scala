package com.chappal.actors

import akka.actor.{ Actor, ActorLogging, ActorRef, Cancellable, Props, Scheduler }
import com.chappal.actors.HttpMessengerActor.{ Ping, PingResponse, ServerStatus }

import scala.collection.mutable.ListMap
import scala.concurrent.Future
import scala.concurrent.duration._
import scala.language.postfixOps

final case class Website(url: String, name: String, mobileNumber: String)

object SiteObserverActor {

  final case object SiteObserverCreated
  final case object Observe
  final case object Resume
  final case object Stop
  final case object NotFound
  final case object ServerList
  final case class GetStatus(name: String)
  final case class Pause(name: String)

}

class SiteObserverActor(url: String, name: String) extends Actor with ActorLogging {

  import SiteObserverActor._
  import akka.pattern.ask
  import akka.util.Timeout
  implicit val timeout = Timeout(3 seconds)
  private var monitoredSites = ListMap[String, Cancellable]()
  implicit val ec = context.dispatcher

  override def receive: Receive = {

    case Observe =>

      val messengerActor = context.actorOf(Props(new HttpMessengerActor(url)), name)
      val scheduler = context.system.scheduler.schedule(
        0 milliseconds,
        5 second,
        messengerActor,
        Ping)

      monitoredSites += (name -> scheduler)
      sender() ! SiteObserverCreated

    case GetStatus(name: String) =>

      monitoredSites.find(_._1 == name) match {
        case Some((x, _)) =>
          val logs: Option[Future[Seq[PingResponse]]] = context.child(x).map(_.ask(ServerStatus).mapTo[Seq[PingResponse]])

          //sender() ! logs
          logs match {
            case Some(f) =>
              sender() ! f //Option[Future[Seq[PingResponse]]]

            case None =>

              log.error("actorRef was not running actually")
              sender() ! Future.successful(Seq[PingResponse]())
          }

        case None =>
          log.info(s"no $name monitoring actor found ")

          sender() ! Seq[PingResponse]()

      }

    case Pause(name: String) => monitoredSites.find(_._1 == name).map {
      case (_, cancellable) => cancellable.cancel()

    }

    case ServerList =>
      log.info(monitoredSites.mkString(","))
      sender() ! monitoredSites
  }

}
