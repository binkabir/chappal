package com.chappal.http

import akka.actor.{ ActorRef, ActorSystem }
import akka.event.Logging

import scala.concurrent.duration._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.MethodDirectives.delete
import akka.http.scaladsl.server.directives.MethodDirectives.get
import akka.http.scaladsl.server.directives.MethodDirectives.post
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import akka.http.scaladsl.server.directives.PathDirectives.path

import scala.concurrent.Future
import com.chappal.actors.HeartBeatReceptionistActor._
import akka.pattern.ask
import akka.util.Timeout
import com.chappal.JsonSupport
import com.chappal.actors.HeartBeatReceptionistActor.CreateObserver
import com.chappal.actors.HttpMessengerActor.PingResponse
import com.chappal.actors.Website
import scala.concurrent.ExecutionContext.Implicits.global

//#user-routes-class
trait HeartBeatReceptionistRoutes extends JsonSupport {
  //#user-routes-class

  // we leave these abstract, since they will be provided by the App
  implicit def system: ActorSystem

  lazy val log = Logging(system, classOf[HeartBeatReceptionistRoutes])

  // other dependencies that UserRoutes use
  def heartBeatReceptionistActor: ActorRef

  // Required by the `ask` (?) method below
  implicit lazy val timeout = Timeout(5.seconds) // usually we'd obtain the timeout from the system's configuration

  def flattenK[A](f: Future[Option[Future[A]]]): Future[Option[A]] =
    f.flatMap({
      case None => Future.successful(None)
      case Some(g) => g.map(Some(_))
    })

  lazy val heatBeatReceptionistRoutes: Route =
    pathPrefix("site") {
      concat(
        //#users-get-delete
        pathEnd {
          concat(
            /*        get {
              val users: Future[Users] =

                (userRegistryActor ? GetUsers).mapTo[Users]
              complete(users)
            }, */
            post {
              entity(as[Website]) { site =>
                val observerCreated: Future[ActionPerformed] =
                  (heartBeatReceptionistActor ? CreateObserver(site)).mapTo[ActionPerformed]
                onSuccess(observerCreated) { performed =>
                  log.info("Created user [{}]: {}", site.name, performed.description)
                  complete((StatusCodes.Created, performed))
                }
              }
            })
        },
        path(Segment) { name =>
          concat(
            get {
              import spray.json.DefaultJsonProtocol._
              val maybeUser: Future[Option[Future[Seq[PingResponse]]]] =
                (heartBeatReceptionistActor ? HeartBeat(name)).mapTo[Option[Future[Seq[PingResponse]]]]

              val v = flattenK(maybeUser)

              onSuccess(v) {
                case Some(res) => complete((StatusCodes.OK, res))
                case _ => complete(ActionPerformed("not found "))
              }
            })
          //#retrieve-user-info
        }) /*,
            delete {
              //#users-delete-logic
              val userDeleted: Future[ActionPerformed] =
                (userRegistryActor ? DeleteUser(name)).mapTo[ActionPerformed]
              onSuccess(userDeleted) { performed =>
                log.info("Deleted user [{}]: {}", name, performed.description)
                complete((StatusCodes.OK, performed))
              }
              //#users-delete-logic
            })
        }) */
      //#users-get-delete
    }
  //#all-routes
}

