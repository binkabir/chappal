package com.chappal.actors

import akka.actor.{ Actor, ActorLogging }
import com.softwaremill.sttp._
import com.softwaremill.sttp.akkahttp.AkkaHttpBackend

import scala.concurrent.Future

object HttpMessengerActor {

  final case object Ping
  final case object ServerStatus
  final case object Created
  final case class PingResponse(statusCode: Int, date: String) extends Product with Serializable
}

class HttpMessengerActor(url: String) extends Actor with ActorLogging {

  import HttpMessengerActor._
  implicit val ec = context.dispatcher

  private var response = Seq[PingResponse]()

  override def receive: Receive = {
    case Ping =>

      sendRequest(url).map { res =>
        res.header("Date").foreach { date: String =>
          response = response :+ PingResponse(res.code, date)

        }
      }
      sender() ! Created

    case ServerStatus =>
      log.info("server status")
      response.foreach(x => log.info(x.toString))
      sender() ! response

  }

  def sendRequest(url: String): Future[Response[String]] = {
    implicit val backend = AkkaHttpBackend()
    val request = sttp.get(uri"$url")
    request.send()
  }

}
