package com.chappal

import akka.actor.{ Actor, ActorSystem, Props }
import akka.testkit.{ ImplicitSender, TestActorRef, TestKit, TestProbe }
import com.chappal.actors.HttpMessengerActor._
import org.scalatest._
import com.chappal.actors.{ HttpMessengerActor, SiteObserverActor }
import com.chappal.actors.SiteObserverActor._

import scala.concurrent.{ Await, Future }
import scala.concurrent.duration._

final class HttpMessengerActorSpec extends WordSpec with BeforeAndAfterAll {

  private implicit val system = ActorSystem("HttpMessengerActorSpec")

  "A HttpMessenger " must {

    val sender = TestProbe()

    implicit val senderRef = sender.ref
    val httpMessenger = system.actorOf(Props(new HttpMessengerActor("https://powershop.ng")))

    "have a  non empty Vector of PingResponse when it receives a Ping message" in {

      httpMessenger ! Ping
      //  httpMessenger ! ServerStatus

      sender.awaitAssert(Created)

    }

    "be return the  PingResponse data log with is a Vector[PingResponse]" in {

      httpMessenger ! Ping
      httpMessenger ! Ping
      httpMessenger ! ServerStatus

      assert(sender.receiveN(2).size == 2)
    }
  }

  override protected def afterAll() = {

    TestKit.shutdownActorSystem(system)
  }
}
